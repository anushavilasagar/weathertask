package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class CheckinDO implements Serializable {
    public String transactionID = "";
    public String driverName = "";
    public int trip = 0;
    public String vehicleCode = "";
    public String vehicleNumber = "";
    public String scheduleDate = "";
    public String siteID = "";
    public String siteDescription = "";
    public String vrID = "";
    public String locationType = "";
    public String vehicleCarrier = "";
    public String unit = "";

    public String aDate = "";
    public String dDate = "";
    public String plate = "";
    public String dTime = "";
    public String aTime    = "";
    public String nShipments = "";
    public String driver = "";
    public String notes          = "";
    public String location          = "";
    public int checkinFlag          = 0;
    public int odometer = 0;
    public String actualCheckin          = "";
    public String actualCheckout          = "";
    public int startRead          = 0;
    public int endRead          = 0;
    public int statusFlag          = 0;

    public String message = "";
    public int status =0;


    public String instructions="";
}
