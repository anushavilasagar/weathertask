package com.tbs.generic.vansales.common;


import com.tbs.generic.vansales.Model.LoadStockDO;

import java.io.File;
import java.util.ArrayList;

public class AppConstants {


    public static ArrayList<LoadStockDO> listStockDO = new ArrayList<>();


    public static String DATABASE_NAME = "generic.sqlite";
    public static String DATABASE_PATH = "";
    public static String Trans_From_Date = "";
    public static String Trans_To_Date = "";
    public static String Trans_Date = "";
    public static final String MeterReadingProduct = "Bulk";
    public static final String FixedQuantityProduct = "Cylinder";
    public static final String ShipmentListView = "Shipment List View";
    public static String EnableShipments = "";
    public static boolean CapturedReturns = false;
    public static final String Goudy_Old_Style_Bold			= "bgs.ttf";
    public static final String Goudy_Old_Style_Italic			    = "bgi.ttf";
    public static final String Goudy_Old_Style_Normal			    = "bgs.ttf";

    public static final String MONTSERRAT_REGULAR_TYPE_FACE			= "montserrat_regular.ttf";

    public static File file2;
    public static File file3;

    public static File file1;
}
