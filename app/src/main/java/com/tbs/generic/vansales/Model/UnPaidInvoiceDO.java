package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class UnPaidInvoiceDO implements Serializable {


    public String invoiceId = "";
    public String accountingDate = "";
    public Double amount = 0.0;
    public double outstandingAmount = 0.0;
    public String outstandingAmountCurrency = "";
    public double paidAmount = 0.0;
    public String paidAmountCurrency = "";
    public int dueDays = 0;


    public String site = "";
    public String siteId = "";
    public String customer = "";
    public String customerId = "";
    public String currency = "";
    public boolean isSelected;



}
