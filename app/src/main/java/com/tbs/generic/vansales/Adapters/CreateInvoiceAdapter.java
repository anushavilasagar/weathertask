package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Activitys.CreateInvoiceActivity;
import com.tbs.generic.vansales.Model.DeliveryDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class CreateInvoiceAdapter extends RecyclerView.Adapter<CreateInvoiceAdapter.MyViewHolder>  {

    private ArrayList<DeliveryDO> invoiceDOS;
    private Context context;





    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSiteName, tvSiteId;
        private LinearLayout llDetails;

        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvSiteName = view.findViewById(R.id.tvInvoiceNumber);



        }
    }


    public CreateInvoiceAdapter(Context context, ArrayList<DeliveryDO> invoiceDoS) {
        this.context = context;
        this.invoiceDOS = invoiceDoS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        DeliveryDO invoiceDO = invoiceDOS.get(position);
        String aMonth = invoiceDO.accountigDate.substring(4, 6);
        String ayear = invoiceDO.accountigDate.substring(0, 4);
        String aDate = invoiceDO.accountigDate.substring(Math.max(invoiceDO.accountigDate.length() - 2, 0));
        holder.tvSiteName.setText("Delivery : "+invoiceDO.deliveryNumber +"\n"+"Accounting Date : "+aDate+"-"+aMonth+"-"+ayear
                +"\n"+"Amount : "+invoiceDO.amount+" "+invoiceDO.currency);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = holder.getAdapterPosition();
                PreferenceUtils preferenceUtils = new PreferenceUtils(context);
                preferenceUtils.saveString(PreferenceUtils.SELECTION_INVOICE_ID, String.valueOf(invoiceDO.deliveryNumber));
                ((CreateInvoiceActivity)context).tvSelection.setText(""+invoiceDO.deliveryNumber);
                ((CreateInvoiceActivity)context). btnInvoice.setBackgroundColor(ContextCompat.getColor(context,R.color.md_green));
                ((CreateInvoiceActivity)context).btnInvoice.setClickable(true);
                ((CreateInvoiceActivity)context). btnInvoice.setEnabled(true);
                ((CreateInvoiceActivity)context).dialog.dismiss();


            }
        });


    }

    @Override
    public int getItemCount() {
        return invoiceDOS.size();
    }

}
