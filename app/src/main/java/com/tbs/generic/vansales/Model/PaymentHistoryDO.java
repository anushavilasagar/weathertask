package com.tbs.generic.vansales.Model;

import java.io.Serializable;

public class PaymentHistoryDO implements Serializable {

    public String paymentNumber       = "";
    public String status           = "";
    public String accountingDate      = "";
    public String amount              = "";
    public String customer            = "";
    public String customerDescription = "";
    public String site                = "";
    public String siteDescription     = "";



}
