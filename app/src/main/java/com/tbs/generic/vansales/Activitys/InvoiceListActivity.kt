package com.tbs.generic.vansales.Activitys

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Adapters.CreatePaymentAdapter
import com.tbs.generic.vansales.Adapters.InvoiceAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.UnPaidInvoicesListRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.util.ArrayList


class InvoiceListActivity : BaseActivity() {
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var loadStockDOs: ArrayList<LoadStockDO>
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var tvNoDataFound: TextView
    lateinit var view: LinearLayout
    var fromId = 0
    private lateinit var siteListRequest: UnPaidInvoicesListRequest

    override fun onResume() {
        super.onResume()
//        selectInvoiceList()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.sales_invoice_list, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()

        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
    }

    override fun initializeControls() {
        tvScreenTitle.text = getString(R.string.select_invoice)
        recycleview = findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.recycleview)
        val linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        tvNoDataFound = findViewById<TextView>(R.id.tvNoDataFound)
        view = findViewById<LinearLayout>(R.id.view)

        if (intent.hasExtra("Sales")) {
            fromId = intent.extras!!.getInt("Sales")
        }
        recycleview.layoutManager = linearLayoutManager

        selectInvoiceList()

        val btnCreate = findViewById<Button>(R.id.btnCreate)

        btnCreate.setOnClickListener {
            Util.preventTwoClick(it)
            val intent = Intent(this@InvoiceListActivity, OnAccountCreatePaymentActivity::class.java)
            intent.putExtra("Sales", 1)
            startActivityForResult(intent, 1)

        }
    }

    private fun selectInvoiceList() {


//        var tvInvoiceId = findViewById(R.id.tvInvoiceId) as TextView
        var tvNoData = findViewById<TextView>(R.id.tvNoDataFound)
        var tvAmount = findViewById<TextView>(R.id.tvAmount)
        var tvAmountLabel = findViewById<TextView>(R.id.tvAmountLabel)


        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        val activeDeliverySavedDo = StorageManager.getInstance(this).getActiveDeliveryMainDo(this)
        val customerDo = StorageManager.getInstance(this).getCurrentSpotSalesCustomer(this)

        if (Util.isNetworkAvailable(this)) {

            val ShipmentType = preferenceUtils.getStringFromPreference(PreferenceUtils.ShipmentType, resources.getString(R.string.checkin_non_scheduled))
            if (ShipmentType == resources.getString(R.string.checkin_non_scheduled)) {

                siteListRequest = UnPaidInvoicesListRequest(customerDo.customer, this@InvoiceListActivity)
            } else {
                siteListRequest = UnPaidInvoicesListRequest(activeDeliverySavedDo.customer, this@InvoiceListActivity)
            }
            siteListRequest.setOnResultListener { isError, unPaidInvoiceMainDO ->
                hideLoader()
                if (unPaidInvoiceMainDO != null && unPaidInvoiceMainDO.unPaidInvoiceDOS.size > 0) {

//                    tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.visibility = View.VISIBLE
                    tvAmount.text = "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency

                    preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, String.format("%.2f", unPaidInvoiceMainDO.totalAmount))
                    preferenceUtils.saveInt(PreferenceUtils.PAYMENT_TYPE, unPaidInvoiceMainDO.type)


                    tvNoData.visibility = View.GONE
//                unPaidInvoiceMainDO
                    if (isError) {
                        hideLoader()
                        hideLoader()

                        Toast.makeText(this@InvoiceListActivity, resources.getString(R.string.error_NoData), Toast.LENGTH_SHORT).show()
                    } else {
                        hideLoader()
                        hideLoader()

                        tvAmount.visibility = View.VISIBLE
                        view.visibility = View.VISIBLE

                        var siteAdapter = CreatePaymentAdapter(this@InvoiceListActivity, unPaidInvoiceMainDO.unPaidInvoiceDOS)
                        recycleview.adapter = siteAdapter
                        tvAmount.text = "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount) + " " + unPaidInvoiceMainDO.unPaidInvoiceDOS.get(0).currency
                        preferenceUtils.saveString(PreferenceUtils.TOTAL_AMOUNT, "" + String.format("%.2f", unPaidInvoiceMainDO.totalAmount))

//                    var siteAdapter = CreatePaymentAdapter(this@CreatePaymentActivity, createPaymentDo)
//                    recyclerView.setAdapter(siteAdapter)f

                    }
                } else {
//                tvInvoiceId.setVisibility(View.VISIBLE)
                    tvAmount.visibility = View.GONE
                    tvAmountLabel.visibility = View.GONE

                    tvNoData.visibility = View.VISIBLE
                    recycleview.visibility = View.GONE
                    view.visibility = View.VISIBLE

                    hideLoader()

                }

            }

            siteListRequest.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), resources.getString(R.string.internet_connection), getString(R.string.ok), "", "", false)

        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == 1) {
            selectInvoiceList()
        }
    }


}