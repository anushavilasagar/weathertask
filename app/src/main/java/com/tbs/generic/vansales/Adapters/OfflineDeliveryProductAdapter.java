package com.tbs.generic.vansales.Adapters;

/**
 * Created by VenuAppasani on 13/12/2018.
 */
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tbs.generic.vansales.Activitys.AddProductActivity;
import com.tbs.generic.vansales.Model.CustomProductDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.database.models.SaveStockDBModel;

import java.util.ArrayList;
import java.util.List;

public class OfflineDeliveryProductAdapter extends RecyclerView.Adapter<OfflineDeliveryProductAdapter.MyViewHolder> {
    private List<SaveStockDBModel> productDOS;
    private Context context;
    private List<String> customProductDOS;
    private AddProductActivity.ProductModifiedListner productModifiedListner;
    List<CustomProductDO> list;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName, tvNumber;
        private LinearLayout llDetails;
        public RelativeLayout rlRemove, rlAdd;
        public EditText tvNumberET;
        public CheckBox selectedCB;
        public ImageView ivRemove, ivAdd;
        public MyViewHolder(View view) {
            super(view);
            llDetails = view.findViewById(R.id.llDetails);
            tvProductName = view.findViewById(R.id.tvProductName);
            ivRemove = view.findViewById(R.id.ivRemove);
            ivAdd = view.findViewById(R.id.ivAdd);
            tvNumber = view.findViewById(R.id.tvNumber);
            tvNumberET = view.findViewById(R.id.tvNumberET);
            selectedCB = view.findViewById(R.id.selectedCB);
        }
    }

    public OfflineDeliveryProductAdapter(Context context, List<SaveStockDBModel> productDOS, AddProductActivity.ProductModifiedListner productModifiedListner) {
        customProductDOS  = new ArrayList<String>();
        this.context = context;
        this.productDOS = productDOS;
        this.productModifiedListner = productModifiedListner;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_product_data, parent, false);
        list = new ArrayList<CustomProductDO>();
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SaveStockDBModel productDO = productDOS.get(position);
        holder.tvProductName.setText(productDO.getItem_id()+"\n"+productDO.getItem_desc());
        // holder.tvNumber.setText("" + productDO.getItem_quantity());
        //holder.selectedCB.
        holder.selectedCB.setChecked(productDOS.get(position).isSelected());
        holder.selectedCB.setTag(productDOS.get(position));
        holder.selectedCB.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                SaveStockDBModel contact = (SaveStockDBModel) cb.getTag();
                contact.setSelected(cb.isChecked());
                productDOS.get(position).setSelected(cb.isChecked());
            }
        });

        holder.tvNumberET.setText("" + productDO.getItem_quantity());
        customProductDOS.add(position, productDO.getItem_quantity());
        holder.ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int normalCount =  (Integer.parseInt(holder.tvNumberET.getText().toString()));
                int actual = (Integer.parseInt(productDOS.get(position).getItem_quantity()));
                int updatedCount = normalCount+1;
                if(normalCount < actual){
                    //  holder.tvNumber.setText("" +updatedCount);
                    holder.tvNumberET.setText("" +updatedCount);

                    customProductDOS.set(position, ""+updatedCount);
                }else{
                    Toast.makeText(context, context.getString(R.string.stock_not_available), Toast.LENGTH_SHORT).show();
                }
                productModifiedListner.updateProductCount(customProductDOS);
            }
        });
        holder.ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( (Integer.parseInt(holder.tvNumberET.getText().toString())==1)){
                    Toast.makeText(context,context.getString(R.string.min_qty_should), Toast.LENGTH_SHORT).show();
                }
                else  if( (Integer.parseInt(holder.tvNumberET.getText().toString())==0)) {
                    Toast.makeText(context, context.getString(R.string.stock_not_available), Toast.LENGTH_SHORT).show();
                }
                else{
                    int quantity = (Integer.parseInt(holder.tvNumberET.getText().toString()));
                    int updatedCount = quantity-1;
                    holder.tvNumberET.setText("" +updatedCount);
                    customProductDOS.set(position, ""+updatedCount);
                }
                productModifiedListner.updateProductCount(customProductDOS);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productDOS.size();
    }
}
