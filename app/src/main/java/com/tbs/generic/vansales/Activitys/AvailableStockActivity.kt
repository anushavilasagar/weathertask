package com.tbs.generic.vansales.Activitys

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.tbs.generic.vansales.Adapters.LoadStockAdapter
import com.tbs.generic.vansales.Model.LoadStockDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.*
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.util.ArrayList

//
class AvailableStockActivity : BaseActivity() {
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    private var scheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var nonScheduleDos: ArrayList<LoadStockDO> = ArrayList()
    private var availableStockDos: ArrayList<LoadStockDO> = ArrayList()

    override fun onResume() {
        super.onResume()
    }

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.available_stock, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener {
            setResult(14, null)
            finish()
        }
    }

    override fun initializeControls() {
        tvScreenTitle.text = resources.getString(R.string.available_stock_title)
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)

        if (scheduleDos == null || scheduleDos.size < 1) {
            loadVehicleStockData()
        } else if (nonScheduleDos == null || nonScheduleDos.size < 1) {
            loadNonVehicleStockData()
        } else {
            var isProductExisted = false
            availableStockDos = nonScheduleDos
            for (i in scheduleDos.indices) {
                for (k in availableStockDos.indices) {
                    if (scheduleDos.get(i).product.equals(availableStockDos.get(k).product, true)) {
                        availableStockDos.get(k).quantity = availableStockDos.get(k).quantity + scheduleDos.get(i).quantity
                        isProductExisted = true
                        break
                    }
                }
                if (isProductExisted) {
                    isProductExisted = false
                    continue
                } else {
                    availableStockDos.add(scheduleDos.get(i))
                }
            }
            if (availableStockDos != null && availableStockDos.size > 0) {
                var loadStockAdapter = LoadStockAdapter(this@AvailableStockActivity, availableStockDos, "Shipments")
                recycleview.adapter = loadStockAdapter
            }

        }


    }

    private fun loadVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {
            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")

//            var scheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")
            if (scheduledRootId.length > 0) {
                val loadVanSaleRequest = CurrentScheduledStockRequest(scheduledRootId, this@AvailableStockActivity)
                showLoader()

                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast(resources.getString(R.string.no_data))
                    } else {
                        hideLoader()

                        scheduleDos = loadStockMainDo.loadStockDOS

                    }
                    loadNonVehicleStockData()
                }
                loadVanSaleRequest.execute()
            } else {
                loadNonVehicleStockData()

            }


        } else {
            showToast(resources.getString(R.string.internet_connection))
        }
    }

    private fun loadNonVehicleStockData() {
        if (Util.isNetworkAvailable(this)) {

            var nonScheduledRootId = "" + preferenceUtils.getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
            if (nonScheduledRootId.length > 0) {
                val loadVanSaleRequest = CurrentNonScheduledStockRequest(nonScheduledRootId, this@AvailableStockActivity)
                showLoader()
                loadVanSaleRequest.setOnResultListener { isError, loadStockMainDo ->
                    hideLoader()
                    if (isError) {
                        showToast(resources.getString(R.string.no_data))
                    } else {
                        hideLoader()

                        nonScheduleDos = loadStockMainDo.loadStockDOS
                        var isProductExisted = false
                        availableStockDos = nonScheduleDos
                        for (i in scheduleDos.indices) {
                            for (k in availableStockDos.indices) {
                                if (scheduleDos.get(i).product.equals(availableStockDos.get(k).product, true)) {
                                    availableStockDos.get(k).quantity = availableStockDos.get(k).quantity + scheduleDos.get(i).quantity
                                    isProductExisted = true
                                    break
                                }
                            }
                            if (isProductExisted) {
                                isProductExisted = false
                                continue
                            } else {
                                availableStockDos.add(scheduleDos.get(i))
                            }

                        }
                        var loadStockAdapter = LoadStockAdapter(this@AvailableStockActivity, availableStockDos, "Shipments")
                        recycleview.adapter = loadStockAdapter

                    }
                }
                loadVanSaleRequest.execute()
            } else {
                var loadStockAdapter = LoadStockAdapter(this@AvailableStockActivity, availableStockDos, "Shipments")
                recycleview.adapter = loadStockAdapter
            }


        } else {
            showToast(resources.getString(R.string.internet_connection))
        }
    }
}