package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.ActiveDeliveryDO;
import com.tbs.generic.vansales.Model.LoanReturnDO;
import com.tbs.generic.vansales.Model.SerialListDO;
import com.tbs.generic.vansales.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class LDSerialAdapter extends RecyclerView.Adapter<LDSerialAdapter.MyViewHolder> {

    private ArrayList<SerialListDO> serialListDOS;
    private Context context;
    private LoanReturnDO trailerSelectionDO;

    public void refreshAdapter(@NotNull ArrayList<SerialListDO> serialListDOS) {
        this.serialListDOS = serialListDOS;

        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvLocation, tvSerial;
        private CheckBox cbSelected;

        public MyViewHolder(View view) {
            super(view);
            tvSerial = itemView.findViewById(R.id.tvSerial);
            cbSelected = itemView.findViewById(R.id.cbSelected);


        }
    }


    public LDSerialAdapter(LoanReturnDO activeDeliveryDO, Context context, ArrayList<SerialListDO> serialListDOS) {
        this.context = context;
        this.serialListDOS = serialListDOS;
        this.trailerSelectionDO = activeDeliveryDO;

    }

    private ArrayList<SerialListDO> selectedDOs;

    public ArrayList<SerialListDO> getSelectedTrailerDOs() {
        return selectedDOs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stock_selection, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SerialListDO serialListDO = serialListDOS.get(position);
        if (selectedDOs == null) {
            selectedDOs = new ArrayList<>();
        }
//        final VRSelectionDO vrSelectionDO = new VRSelectionDO();
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lg));
        } else {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lsg));
        }
//        holder.tvLocation.setText("");
        holder.tvSerial.setText(serialListDO.serialDO);

        holder.cbSelected.setOnCheckedChangeListener(null);

        if (serialListDO.state == 1) {
            holder.cbSelected.setChecked(true);
            selectedDOs.add(serialListDO);
            serialListDO.ischecked = true;
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background2));


        } else {
            holder.cbSelected.setChecked(false);

        }
        holder.cbSelected.setChecked(serialListDO.ischecked);
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (serialListDO.state != 1) {
                    if (isChecked) {
                        if (selectedDOs.size() < trailerSelectionDO.qty) {
                            serialListDO.ischecked = true;

                            selectedDOs.add(serialListDO);
                        } else {
                            serialListDO.ischecked = false;

                            holder.cbSelected.setChecked(false);
//                        holder.cbSelected.setEnabled(false);

                            ((BaseActivity) context).showToast(context.getResources().getString(R.string.cannotselect));

                        }


                    } else {
                        serialListDO.ischecked = false;
                        selectedDOs.remove(serialListDO);
                    }
                } else {
                    ((BaseActivity) context).showToast(context.getResources().getString(R.string.reallocatedoc));
                    holder.cbSelected.setChecked(true);

                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return serialListDOS.size();
//        return 10;

    }

}
