package com.tbs.generic.vansales.Requests;

/**
 * Created by VenuAppasani on 06-01-2019
 */

import android.content.Context;
import android.os.AsyncTask;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Model.InvoiceNumberDos;
import com.tbs.generic.vansales.Model.PaymentPdfDO;
import com.tbs.generic.vansales.common.WebServiceAcess;
import com.tbs.generic.vansales.utils.AppPrefs;
import com.tbs.generic.vansales.utils.Constants;
import com.tbs.generic.vansales.utils.PreferenceUtils;
import com.tbs.generic.vansales.utils.ServiceURLS;
import com.tbs.generic.vansales.utils.WebServiceConstants;

import org.json.JSONObject;
import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class PDFPaymentDetailsRequest extends AsyncTask<String, Void, Boolean> {

    private PaymentPdfDO paymentPdfDO;
    private InvoiceNumberDos invoiceNumberDos;

    private Context mContext;
    private String id;
    String username, password, ip, pool, port;
    PreferenceUtils preferenceUtils;

    public PDFPaymentDetailsRequest(String id, Context mContext) {
        this.mContext = mContext;
        this.id = id;
    }

    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

    OnResultListener onResultListener;

    public interface OnResultListener {
        void onCompleted(boolean isError, PaymentPdfDO createInvoiceDO);

    }

    public boolean runRequest() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("I_XPAYNUM", id);

        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        WebServiceAcess webServiceAcess = new WebServiceAcess();
        String resultXML = webServiceAcess.runRequest(mContext, ServiceURLS.runAction, WebServiceConstants.PAYMENT_DETAILS, jsonObject);
        if (resultXML != null && resultXML.length() > 0) {
            return parseXML(resultXML);
        } else {
            return false;
        }
    }

    public boolean parseXML(String xmlString) {
        System.out.println("xmlString " + xmlString);
        try {
            String text = "", attribute = "", startTag = "", endTag = "";
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(xmlString));
            int eventType = xpp.getEventType();

            paymentPdfDO = new PaymentPdfDO();

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {

                    startTag = xpp.getName();
                    if (startTag.equalsIgnoreCase("FLD")) {
                        attribute = xpp.getAttributeValue(null, "NAME");
                    } else if (startTag.equalsIgnoreCase("GRP")) {

                    } else if (startTag.equalsIgnoreCase("TAB")) {
                        paymentPdfDO.invoiceDos = new ArrayList<>();

                    } else if (startTag.equalsIgnoreCase("LIN")) {
                        invoiceNumberDos = new InvoiceNumberDos();

                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    endTag = xpp.getName();

                    if (endTag != null && startTag.equalsIgnoreCase("FLD")) {

                        if (attribute.equalsIgnoreCase("I_XPAYNUM")) {
                            paymentPdfDO.paymentNumber = text;
                        } else if (attribute.equalsIgnoreCase("O_XBPNAM")) {
                            paymentPdfDO.customer = text;
                        } else if (attribute.equalsIgnoreCase("O_XBPRSHO")) {
                            paymentPdfDO.customerDescription = text;
                        } else if (attribute.equalsIgnoreCase("O_XCPYIMG")) {
                            if (text.length() > 0) {
                                paymentPdfDO.logo = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XUSERID")) {
                            if (text.length() > 0) {
                                paymentPdfDO.createUserID = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XUSER")) {
                            if (text.length() > 0) {
                                paymentPdfDO.createUserName = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEDES")) {
                            if (text.length() > 0) {

                                paymentPdfDO.siteDescription = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XCPYNAM")) {
                            paymentPdfDO.companyCode = text;

                        } else if (attribute.equalsIgnoreCase("O_XCPYDES")) {
                            if (text.length() > 0) {

                                paymentPdfDO.company = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG0")) {
                            if (text.length() > 0) {

                                paymentPdfDO.siteAddress1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG1")) {
                            if (text.length() > 0) {

                                paymentPdfDO.siteAddress2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XADDLIG0")) {

                            paymentPdfDO.customerStreet = text;

                        } else if (attribute.equalsIgnoreCase("O_XADDLIG1")) {


                            paymentPdfDO.customerLandMark = text;
                        } else if (attribute.equalsIgnoreCase("O_XADDLIG2")) {


                            paymentPdfDO.customerTown = text;
                        } else if (attribute.equalsIgnoreCase("O_XCTY")) {


                            paymentPdfDO.customerCity = text;
                        } else if (attribute.equalsIgnoreCase("O_XPOS")) {


                            paymentPdfDO.customerPostalCode = text;
                        } else if (attribute.equalsIgnoreCase("O_XSITADDLIG2")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteAddress3 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCRY")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteCountry = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITCTY")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteCity = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITPOS")) {
                            if (text.length() > 0) {
                                paymentPdfDO.sitePostalCode = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITLND")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteLandLine = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITMOB")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteMobile = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITFAX")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteFax = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML1")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteEmail1 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITEML2")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteEmail2 = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XSITWEB")) {
                            if (text.length() > 0) {
                                paymentPdfDO.siteWebEmail = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XCRY")) {
                            paymentPdfDO.country = text;
                        } else if (attribute.equalsIgnoreCase("O_XLND")) {
                            paymentPdfDO.landline = text;
                        } else if (attribute.equalsIgnoreCase("O_XMOB")) {
                            paymentPdfDO.mobile = text;
                        } else if (attribute.equalsIgnoreCase("O_XFAX")) {
                            paymentPdfDO.fax = text;
                        } else if (attribute.equalsIgnoreCase("O_XEML1")) {
                            paymentPdfDO.mail = text;
                        } else if (attribute.equalsIgnoreCase("O_XAMT")) {
                            paymentPdfDO.amount = Double.valueOf(text);
                        } else if (attribute.equalsIgnoreCase("O_XCUR")) {
                            paymentPdfDO.currency = text;
                        } else if (attribute.equalsIgnoreCase("O_XBANK")) {
                            if (text.length() > 0) {
                                paymentPdfDO.bank = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XCHKNUM")) {
                            paymentPdfDO.chequeNumber = text;
                        } else if (attribute.equalsIgnoreCase("O_XCREDAT")) {
                            if (text.length() > 0) {
                                paymentPdfDO.createdDate = text;
                            }
                        }
//                        else if (attribute.equalsIgnoreCase("O_XCHKDAT")) {
//                            paymentPdfDO.chequeDate=text;
//                        }
                        else if (attribute.equalsIgnoreCase("O_XDAT")) {
                            if (text.length() > 0) {
                                paymentPdfDO.chequeDate = text;
                            }
                        } else if (attribute.equalsIgnoreCase("O_XINVNUM")) {
                            if (text.length() > 0) {
                                invoiceNumberDos.invoiceNumber = text;

                            }
                        } else if (attribute.equalsIgnoreCase("O_XFLG")) {
                            paymentPdfDO.flag = text;
                        } else if (attribute.equalsIgnoreCase("O_XCRETIM")) {
                            if (text.length() > 0) {
                                paymentPdfDO.createdTime = text;
                            }

                        }
//                        else if (attribute.equalsIgnoreCase("O_YARDAT")) {
//                            createInvoiceDO.arrivalDate=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YARDAT")) {
//                            createInvoiceDO.estimatedDate=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YARTIM")) {
//                            createInvoiceDO.estimatedTime=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YITM")) {
//                            createInvoiceDO.product=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YITMDES")) {
//                            createInvoiceDO.productDesccription=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YDQTY")) {
//                            createInvoiceDO.deliveredQunatity=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YGRI")) {
//                            createInvoiceDO.grossPrice=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_NPRI")) {
//                            createInvoiceDO.netPrice=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YCPRI")) {
//                            createInvoiceDO.costPrice=text;
//                        }
//                        else if (attribute.equalsIgnoreCase("O_YMRG")) {
//                            createInvoiceDO.margin=text;
//                        }
                    }

                    if (endTag.equalsIgnoreCase("GRP")) {
                        // customerDetailsMainDo.customerDetailsDos.add(customerDetailsDo);
                    }

                    if (endTag.equalsIgnoreCase("LIN")) {
                        paymentPdfDO.invoiceDos.add(invoiceNumberDos);
                    }
                    text = "";
                } else if (eventType == XmlPullParser.TEXT) {
                    text = xpp.getText();
                }

                eventType = xpp.next();
            }
            return true;
        } catch (Exception e) {
            System.out.println("Exception Parser" + e);

            return false;
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        ((BaseActivity) mContext).showLoader();
    }

    @Override
    protected Boolean doInBackground(String... param) {
        return runRequest();
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);

        ((BaseActivity) mContext).hideLoader();
        if (onResultListener != null) {
            onResultListener.onCompleted(!result, paymentPdfDO);
        }
    }
}