package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbs.generic.vansales.Model.InvoiceHistoryDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.utils.PreferenceUtils;

import java.util.ArrayList;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder>{
    int count=0;
    private ArrayList<InvoiceHistoryDO> invoiceDOS;
    private String imageURL;
    private Context context;
    private PreferenceUtils preferenceUtils;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvInvoiceNumber;
        public RelativeLayout rlRemove,rlAdd;
        public ImageView ivRemove,ivAdd;

        public MyViewHolder(View view) {
            super(view);
            tvInvoiceNumber   = view.findViewById(R.id.tvInvoiceNumber);

        }
    }


    public InvoiceAdapter(Context context, ArrayList<InvoiceHistoryDO> invoiceDOS) {
        this.context = context;
        this.invoiceDOS = invoiceDOS;

        preferenceUtils = new PreferenceUtils(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_data, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final InvoiceHistoryDO invoiceHistoryDO =invoiceDOS.get(position);
        String aMonth = invoiceHistoryDO.accountingDate.substring(4, 6);
        String ayear = invoiceHistoryDO.accountingDate.substring(0, 4);
        String aDate = invoiceHistoryDO.accountingDate.substring(Math.max(invoiceHistoryDO.accountingDate.length() - 2, 0));
        holder.tvInvoiceNumber.setText(context.getString(R.string.invoice)+" : "+invoiceHistoryDO.invoiceNumber +"\n"+context.getString(R.string.amount)+" : "+invoiceHistoryDO.amount+" " +invoiceHistoryDO.currency
                +"\n"+context.getString(R.string.date)+" : "+aDate+"-"+aMonth+"-"+ayear);
//        holder.tvDescription.setText(loadStockDO.productDescription);
//        holder.tvNumber.setText(""+loadStockDO.itemCount);




    }

    @Override
    public int getItemCount() {
        return invoiceDOS!=null?invoiceDOS.size():0;
    }

}
