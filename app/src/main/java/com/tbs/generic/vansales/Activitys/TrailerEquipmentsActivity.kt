package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.tbs.generic.vansales.Adapters.ScheduledProductsAdapter
import com.tbs.generic.vansales.Model.ActiveDeliveryDO
import com.tbs.generic.vansales.Model.NonScheduledProductMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.CurrentNonScheduledStockRequest
import com.tbs.generic.vansales.Requests.TrailerEquipmentStockRequest
import com.tbs.generic.vansales.common.AppConstants
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.DBProductsListener
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import java.util.*


class TrailerEquipmentsActivity : BaseActivity(), DBProductsListener {
    override fun updateData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    lateinit var tvSelection: TextView
    lateinit var tvNoDataFound: TextView
    lateinit var dialog: Dialog
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var llOrderHistory: RelativeLayout
    lateinit var customerId: String
    private lateinit var btnAdd: Button
     var loadStockAdapter: ScheduledProductsAdapter? = null
    lateinit var tvScreenTitles: TextView
    var tvNoOrders: TextView? = null
    lateinit var loadStockDoS: ArrayList<ActiveDeliveryDO>
    lateinit var llSearch: LinearLayout
    lateinit var etSearch: EditText
    lateinit var ivClearSearch: ImageView
    lateinit var ivSearchs: ImageView
    lateinit var ivGoBack: ImageView
    lateinit var routeId: String
    lateinit var productDOs: ArrayList<ActiveDeliveryDO>
    lateinit var fromm: String

    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.scheduled_add_product_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        flToolbar.visibility = View.GONE
        disableMenuWithBackButton()
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        disableMenuWithBackButton()
        routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras?.getString("CustomerId")!!
        }

        initializeControls()
        if (intent.hasExtra("FROM")) {
            fromm = intent.extras?.getString("FROM")!!
        }
        loadNonScheduleProducts()
        btnAdd.setOnClickListener {
            if (loadStockAdapter != null) {
                Util.preventTwoClick(it)
                var isProductsAdded: Boolean? = false
                if (loadStockAdapter != null && loadStockAdapter!!.getSelectedactiveDeliveryDOs() != null && loadStockAdapter!!.getSelectedactiveDeliveryDOs().size > 0) {
                    for (i in loadStockAdapter!!.getSelectedactiveDeliveryDOs().indices) {
                        if (loadStockAdapter!!.getSelectedactiveDeliveryDOs().get(i).isProductAdded && loadStockAdapter!!.getSelectedactiveDeliveryDOs().get(i).orderedQuantity > 0) {
                            isProductsAdded = true
                            break
                        }
                    }
                }
                if (isProductsAdded!!) {
                    showAppCompatAlert("", getString(R.string.are_you_sure_you_want_add_products), getString(R.string.ok), getString(R.string.cancel), getString(R.string.success), true)


                } else {
                    showToast(getString(R.string.please_select_anu_pro))
                }
            }
        }

        ivGoBack.setOnClickListener {
            Util.preventTwoClick(it)
            finish()
        }
        ivSearch.setOnClickListener {
            Util.preventTwoClick(it)
            //                if(llSearch.visibility == View.VISIBLE){
            //                llSearch.visibility = View.INVISIBLE
            //            }
            //            else{
            //            }
            tvScreenTitles.visibility = View.GONE
            llSearch.visibility = View.VISIBLE
        }

        ivClearSearch.setOnClickListener {
            Util.preventTwoClick(it)
            etSearch.setText("")
            if (loadStockDoS != null && loadStockDoS.size > 0) {
                loadStockAdapter = ScheduledProductsAdapter(this@TrailerEquipmentsActivity, loadStockDoS, "AddProducts")
                recycleview.adapter = loadStockAdapter
                tvNoDataFound.visibility = View.GONE
                recycleview.visibility = View.VISIBLE
            } else {
                tvNoDataFound.visibility = View.VISIBLE
                recycleview.visibility = View.GONE
            }
        }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                if (etSearch.text.toString().trim { it <= ' ' }.equals("", ignoreCase = true)) {
                    if (loadStockDoS != null && loadStockDoS.size > 0) {
                        loadStockAdapter = ScheduledProductsAdapter(this@TrailerEquipmentsActivity, loadStockDoS, "AddProducts")
                        recycleview.adapter = loadStockAdapter
                        tvNoDataFound.visibility = View.GONE
                        recycleview.visibility = View.VISIBLE
                    } else {
                        tvNoDataFound.visibility = View.VISIBLE
                        recycleview.visibility = View.GONE
                    }
                } else if (etSearch.text.toString().trim { it <= ' ' }.length > 2) {
                    filter(etSearch.text.toString().trim { it <= ' ' })
                }
            }
        })

    }

    private fun filter(filtered: String): ArrayList<ActiveDeliveryDO> {
        productDOs = ArrayList()
        for (i in loadStockDoS.indices) {
            if (loadStockDoS.get(i).product.contains(filtered) || loadStockDoS.get(i).productDescription.contains(filtered)) {
                productDOs.add(loadStockDoS.get(i))
            }
        }
        if (productDOs.size > 0) {
            loadStockAdapter = ScheduledProductsAdapter(this@TrailerEquipmentsActivity, loadStockDoS, "AddProducts")
            recycleview.adapter = loadStockAdapter
            //            productAdapter.refreshAdapter(productDOs);
            tvNoDataFound.visibility = View.GONE
            recycleview.visibility = View.VISIBLE
        } else {
            tvNoDataFound.visibility = View.VISIBLE
            recycleview.visibility = View.GONE
        }
        return productDOs
    }

    private fun loadNonScheduleProducts() {
        var routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, "")

        if (Util.isNetworkAvailable(this)) {

            val request = TrailerEquipmentStockRequest(routeId, this@TrailerEquipmentsActivity)
            showLoader()
            request.setOnResultListener { isError, requestDo ->
                hideLoader()
                if (isError) {
                    showToast(getString(R.string.no_vehicle_data_found))
                } else {
                    hideLoader()
//                        StorageManager.getInstance(this).saveNonScheduledVehicleStockInLocal(loadStockMainDo.loadStockDOS!!)

                    if (requestDo.trailerSelectionDOS != null && requestDo.trailerSelectionDOS.size > 0) {
                        recycleview.visibility = VISIBLE
                        tvNoDataFound.visibility = GONE
                        btnAdd.visibility = VISIBLE
                        loadStockDoS = ArrayList<ActiveDeliveryDO>()
                        for (i in requestDo.trailerSelectionDOS.indices) {
                            val loadStockDO = requestDo.trailerSelectionDOS.get(i)
                            val activeDeliveryDO = ActiveDeliveryDO()
//                            activeDeliveryDO.shipmentId = loadStockDO.shipmentNumber
                            activeDeliveryDO.product = loadStockDO.trailer
                            activeDeliveryDO.productDescription = loadStockDO.trailerDescription
                            activeDeliveryDO.stockUnit = loadStockDO.volumeUnit
                            activeDeliveryDO.weightUnit = loadStockDO.massUnit
                            activeDeliveryDO.quantity = 1
                            activeDeliveryDO.weightUnit = "" + loadStockDO.massUnit
                            activeDeliveryDO.unit = loadStockDO.volumeUnit
                            activeDeliveryDO.totalQuantity = 1
                            activeDeliveryDO.orderedQuantity = 1
                            activeDeliveryDO.productUnit = loadStockDO.volumeUnit

                            activeDeliveryDO.shipmentProductType = AppConstants.FixedQuantityProduct

                            loadStockDoS.add(activeDeliveryDO)
                        }
                        loadStockAdapter = ScheduledProductsAdapter(this@TrailerEquipmentsActivity, loadStockDoS, "AddProducts")
                        recycleview.adapter = loadStockAdapter
                    }

                }
            }
            request.execute()
        } else {
            showAppCompatAlert(getString(R.string.alert), getString(R.string.please_check_internet), getString(R.string.ok), "", getString(R.string.failure), false)

        }


    }

    override fun initializeControls() {

        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        tvNoDataFound = findViewById<View>(R.id.tvNoDataFound) as TextView
        btnAdd = findViewById<View>(R.id.btnAdd) as Button
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this, androidx.recyclerview.widget.LinearLayoutManager.VERTICAL, false)
        tvScreenTitles = findViewById<View>(R.id.tvScreenTitles) as TextView
        llSearch = findViewById<LinearLayout>(R.id.llSearch)
        etSearch = findViewById<EditText>(R.id.etSearch)
        ivClearSearch = findViewById<ImageView>(R.id.ivClearSearch)
        ivGoBack = findViewById<ImageView>(R.id.ivGoBack)
        ivSearch = findViewById(R.id.ivSearchs)

    }

    override fun onButtonYesClick(from: String) {

        if (getString(R.string.success).equals(from, ignoreCase = true)) {
            val loadStockDos = loadStockAdapter!!.getSelectedactiveDeliveryDOs()
            val intent = Intent()
            intent.putExtra("AddedProducts", loadStockDos)

            setResult(1, intent)
            finish()
        } else
            if (getString(R.string.failure).equals(from, ignoreCase = true)) {


                //finish()
            }

    }
}