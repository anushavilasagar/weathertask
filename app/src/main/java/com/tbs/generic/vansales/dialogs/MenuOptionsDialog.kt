package com.tbs.generic.vansales.dialogs

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.tbs.generic.pod.Activitys.MasterDashboardActivity
import com.tbs.generic.vansales.Activitys.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.LogoutTimeCaptureRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.*
import com.tbs.generic.vansales.utils.Util.showToast
import kotlinx.android.synthetic.main.dailog_menu_options.*


class MenuOptionsDialog : BottomSheetDialogFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dailog_menu_options, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_back.setOnClickListener {
            dismiss()
        }

        tv_menu.setOnClickListener { dismiss() }
        //User Activity Report
        user_activity_report.setOnClickListener {

            val vehicleCheckInDo = StorageManager.getInstance(activity).getVehicleCheckInData(activity)
            val status = vehicleCheckInDo.checkInStatus
            if (!status!!.isEmpty()) {
                val intent = Intent(activity, UserActivityReportActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
            } else {
                //showAppCompatAlert("Alert!", "Please Finish Check-in Process", "OK", "", "FAILURE", false)

                YesOrNoDialogFragment().newInstance(getString(R.string.alert), getString(R.string.please_finish_check_in_process), ResultListner { `object`, isSuccess ->


                }, false).show(activity?.supportFragmentManager!!, "YesOrNoDialogFragment")
            }

        }


        //Master Data
        master_data.setOnClickListener {
            val intent = Intent(activity, MasterDashboardActivity::class.java)
            startActivity(intent)
        }

        //lLogout

        log_out.setOnClickListener {
            logOutTimeCapture()

        }


        vehicle_stock.setOnClickListener {

            val vehicleCheckInDo = StorageManager.getInstance(activity).getVehicleCheckInData(activity)
            if (!vehicleCheckInDo.loadStock.equals("")) {
                val intent = Intent(activity, VehicleStcokActivity::class.java)
                startActivity(intent)
            } else {
                YesOrNoDialogFragment().newInstance(getString(R.string.alert), getString(R.string.please_finish_check_in_process), ResultListner { `object`, isSuccess ->


                }, false).show(activity?.supportFragmentManager!!, "YesOrNoDialogFragment")
            }

        }

        opening_stock.setOnClickListener {
            Util.preventTwoClick(it)
            val vehicleCheckInDo = StorageManager.getInstance(activity).getVehicleCheckInData(activity)
            if (!vehicleCheckInDo.checkInStatus!!.isEmpty()) {
                if (!vehicleCheckInDo.loadStock.equals("")) {
                    /* val intent = Intent(context@DashBoardActivity, LoadStockStatusActivity::class.java)
                     intent.putExtra("FLAG", 1);
                     intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                     intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                     startActivity(intent)*/
                    val intent = Intent(activity, ScheduleNonScheduleActivity::class.java)
                    intent.putExtra(Constants.SCREEN_TYPE, getString(R.string.opening_stock))
                    startActivity(intent)
                } else {

                    showToast(activity, getString(R.string.please_complete_stock_load))
                }
            } else {

                if (!vehicleCheckInDo.loadStock.equals("")) {
                    /*val intent = Intent(context@DashBoardActivity, LoadStockStatusActivity::class.java)
                    intent.putExtra("FLAG", 1);
                    intent.putExtra("ScreenTitle", "Load Van Sales Stock")
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)*/
                    val intent = Intent(activity, ScheduleNonScheduleActivity::class.java)
                    intent.putExtra(Constants.SCREEN_TYPE, getString(R.string.opening_stock))
                    startActivity(intent)
                } else {
                    YesOrNoDialogFragment().newInstance(getString(R.string.alert), getString(R.string.please_finish_check_in_process), ResultListner { `object`, isSuccess ->


                    }, false).show(activity?.supportFragmentManager!!, "YesOrNoDialogFragment")
                }

            }
        }

    }


    private fun logOutTimeCapture() {
        val preferenceUtils = PreferenceUtils(context)
        val driverId = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
        val date = CalendarUtils.getDate()
        val time = CalendarUtils.getTime()

        val siteListRequest = LogoutTimeCaptureRequest(driverId, date, date, time, activity)
        siteListRequest.setOnResultListener { isError, loginDo ->

            if (isError) {
                showToast(activity, getString(R.string.you_are_logout))
            } else {

                if (loginDo != null) {
                    if (loginDo.flag == 2) {
                        showToast(activity, getString(R.string.you_have_successfully_logout))
                        preferenceUtils.removeFromPreference(PreferenceUtils.DRIVER_ID)
                        preferenceUtils.removeFromPreference(PreferenceUtils.LOGIN_SUCCESS)

                        StorageManager.getInstance(activity).deleteSpotSalesCustomerList(activity)
                        AppPrefs.clearPref(activity)
                        clearDataLocalData()


                    } else {
                        showToast(activity, getString(R.string.unable_to_logout))

                    }
                } else {
                    showToast(activity, getString(R.string.unable_to_logout))


                }
            }

        }

        siteListRequest.execute()
    }
    private fun clearDataLocalData() {
        val preferenceUtils = PreferenceUtils(context)
        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_READ)
        preferenceUtils.removeFromPreference(PreferenceUtils.SCHEDULE_DOCUMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.INSTRUCTIONS)
        preferenceUtils.removeFromPreference(PreferenceUtils.OPERATION)
        preferenceUtils.removeFromPreference(PreferenceUtils.SPOT_RECEIPT)
        preferenceUtils.removeFromPreference(PreferenceUtils.PRODUCT_CODE);

        preferenceUtils.removeFromPreference(PreferenceUtils.CHECKIN_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CUSTOMER)
        preferenceUtils.removeFromPreference(PreferenceUtils.LATTITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LONGITUDE)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.CARRIER_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.NOTES)
        preferenceUtils.removeFromPreference(PreferenceUtils.OPENING_READING)
        preferenceUtils.removeFromPreference(PreferenceUtils.ODO_UNIT)
        preferenceUtils.removeFromPreference(PreferenceUtils.START_ROUTE)
        preferenceUtils.removeFromPreference(PreferenceUtils.PICKUP_DROP_FLAG)
        preferenceUtils.removeFromPreference(PreferenceUtils.DOC_TYPE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOAD_BAY_VALUE)
        preferenceUtils.removeFromPreference(PreferenceUtils.LOAD_BAY)
        preferenceUtils.removeFromPreference(PreferenceUtils.CLOSING_READING)
        preferenceUtils.removeFromPreference(PreferenceUtils.BASE_VEHICLE_ROUTE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.INVOICE_ID)
        preferenceUtils.removeFromPreference(PreferenceUtils.SHIPMENT)
        preferenceUtils.removeFromPreference(PreferenceUtils.SITE_NAME)
        preferenceUtils.removeFromPreference(PreferenceUtils.VOLUME)
        preferenceUtils.removeFromPreference(PreferenceUtils.WEIGHT)
        preferenceUtils.removeFromPreference(PreferenceUtils.CV_PLATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CA_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_DATE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CVEHICLE_CODE)
        preferenceUtils.removeFromPreference(PreferenceUtils.CD_TIME)
        preferenceUtils.removeFromPreference(PreferenceUtils.CN_SHIPMENTS)
        preferenceUtils.removeFromPreference(PreferenceUtils.CHECK_IN_STATUS)
        preferenceUtils.removeFromPreference(PreferenceUtils.TOTAL_LOAD)
        preferenceUtils.removeFromPreference(PreferenceUtils.VEHICLE_CAPACITY)
        preferenceUtils.removeFromPreference(PreferenceUtils.SELECTION)
        AppPrefs.clearPref(context)
        StorageManager.getInstance(context).deleteCheckInData(context as Activity?)
        StorageManager.getInstance(context).deleteCompletedShipments(context as Activity?)
        StorageManager.getInstance(context).deleteSkipShipmentList(context as Activity?)
        StorageManager.getInstance(context).deleteVanNonScheduleProducts(context as Activity?)
        StorageManager.getInstance(context).deleteVanScheduleProducts(context as Activity?)
        StorageManager.getInstance(context).deleteSpotSalesCustomerList(context as Activity?)
        StorageManager.getInstance(context).deleteVehicleInspectionList(context as Activity?)
        StorageManager.getInstance(context).deleteGateInspectionList(context as Activity?)
        StorageManager.getInstance(context).deleteSiteListData(context as Activity?)
        StorageManager.getInstance(context).deleteCurrentSpotSalesCustomer(context)
        StorageManager.getInstance(context).deleteTrailerSelectionDO(context as Activity?)
        StorageManager.getInstance(context).deleteEquipmentSelectionDO(context as Activity?)
        StorageManager.getInstance(context).deleteImagesData(context as Activity?)

        StorageManager.getInstance(context).deleteLoanDeliveryImagesData(context as Activity?)
        StorageManager.getInstance(context).deleteLoanReturnImagesData(context as Activity?)
        StorageManager.getInstance(context as Activity?).deleteTrailerSelectionDO(context as Activity?)
        StorageManager.getInstance(context as Activity?).deleteEquipmentSelectionDO(context as Activity?)
        StorageManager.getInstance(context).deleteShipmentListData(context as Activity?)
        StorageManager.getInstance(context).deleteCheckOutData(context)
        StorageManager.getInstance(context).deleteDepartureData(context as Activity?)
        StorageManager.getInstance(context).deleteUpdateData(context as Activity?)
        StorageManager.getInstance(context as Activity?).deleteVehCheckOutData(context as Activity?)
        StorageManager.getInstance(context as Activity?).deleteSerialEquipmentSelectionDO(context as Activity?)

        StorageManager.getInstance(context).deleteScheduledNonScheduledReturnData()// clearing all tables
//        preferenceUtils.saveString(PreferenceUtils.STATUS, "" + resources.getString(R.string.logged_in))
//        preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, resources.getString(R.string.logged_in))
        val intent = Intent(activity, LoginActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }
}