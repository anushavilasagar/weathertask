package com.tbs.generic.vansales.Activitys;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import androidx.core.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.tbs.generic.vansales.Model.SuccessDO;
import com.tbs.generic.vansales.R;
import com.tbs.generic.vansales.Requests.CustomersLocationRequest;
import com.tbs.generic.vansales.common.WorkaroundMapFragment;

import java.io.IOException;
import java.util.List;

public class CustomerLocationActivity extends BaseActivity implements OnMapReadyCallback, LocationListener {

    GoogleMap map;
    LocationManager locationManager;
    private RelativeLayout llOrderHistory;
    private WorkaroundMapFragment mapFragment;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    private LatLng latLng;

    private void configureCameraIdle() {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                try {
                    latLng = map.getCameraPosition().target;
                    Geocoder geocoder = new Geocoder(CustomerLocationActivity.this);
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        String locality = addressList.get(0).getAddressLine(0);
                        String country = addressList.get(0).getCountryName();
                        if (!locality.isEmpty() && !country.isEmpty())
                            captureCustomerLocationRequest("", latLng.latitude, latLng.longitude, getApplicationContext());
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }

    @Override
    public void initialize() {
        llOrderHistory = (RelativeLayout) getLayoutInflater().inflate(R.layout.customer_location, null);
        llBody.addView(llOrderHistory, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        changeLocale();
        initializeControls();
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestSingleUpdate(criteria, CustomerLocationActivity.this, Looper.getMainLooper());

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        locationManager.requestSingleUpdate(criteria, CustomerLocationActivity.this, Looper.getMainLooper());

        configureCameraIdle();
        mapFragment = (WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mapFragment.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                llBody.requestDisallowInterceptTouchEvent(true);
            }
        });

//        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
//
//        mapFragment.getMapAsync(this);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        //  map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
//
//        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
//
//        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return;
//        }
//        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1, 1, (LocationListener) this);
//

    }

    @Override
    public void initializeControls() {

        tvScreenTitle.setText(R.string.customer_location);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            map = googleMap;
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            map.getUiSettings().setZoomControlsEnabled(true);
            map.getUiSettings().setScrollGesturesEnabled(true);
            map.getUiSettings().setRotateGesturesEnabled(true);
            map.setOnCameraIdleListener(onCameraIdleListener);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            map.setMyLocationEnabled(true);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onLocationChanged(Location location) {
//        map.clear();
//
//        //To hold location
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//
//        //To create marker in map
//        MarkerOptions markerOptions = new MarkerOptions();
//        markerOptions.position(latLng);
//        markerOptions.title("My Location");
//        //adding marker to the map
//        map.addMarker(markerOptions);
//
//        //opening position with some zoom level in the map
//        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
//        captureCustomerLocationRequest("",location.getLatitude(), location.getLongitude(), this);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.0f));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
    private void captureCustomerLocationRequest(String id, Double lat, Double lng, Context context) {
        CustomersLocationRequest driverListRequest = new CustomersLocationRequest(id, lat, lng, context);
        driverListRequest.setOnResultListener(new CustomersLocationRequest.OnResultListener() {
            @Override
            public void onCompleted(boolean isError, SuccessDO customerDetailsMainDo) {
                hideLoader();

                if (isError) {
                    hideLoader();

                    Toast.makeText(CustomerLocationActivity.this, R.string.error_details_list, Toast.LENGTH_SHORT).show();
                } else {
                    hideLoader();
                    if (customerDetailsMainDo != null) {
                        if (isError) {
                            showToast("failure");
                        } else {
                            if (customerDetailsMainDo.flag == 2) {
                                showToast("Location Updated");

                            } else {
                                showToast("Location not updated");

                            }

                        }
                    }

                }
            }

        });
        driverListRequest.execute();

    }
}
