package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/7/2018.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tbs.generic.vansales.Activitys.BaseActivity;
import com.tbs.generic.vansales.Activitys.PurchaseRecieptInfoActivity;
import com.tbs.generic.vansales.Activitys.SerializationActivity;
import com.tbs.generic.vansales.Model.TrailerSelectionDO;
import com.tbs.generic.vansales.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class EquipmentSelectionAdapter extends RecyclerView.Adapter<EquipmentSelectionAdapter.MyViewHolder> {

    private ArrayList<TrailerSelectionDO> trailerSelectionDOS;
    private Context context;


    public void refreshAdapter(@NotNull ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.trailerSelectionDOS = trailerSelectionDOS;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTrailer, tvDescription, tvType, tvAxle, tvMass, tvVolume;
        private CheckBox cbSelected;
        private Button btnSerial;


        public MyViewHolder(View view) {
            super(view);
            tvTrailer = itemView.findViewById(R.id.tvTrailer);
            tvAxle = itemView.findViewById(R.id.tvAxle);
            tvMass = itemView.findViewById(R.id.tvMass);
            tvVolume = itemView.findViewById(R.id.tvVolume);
            tvType = itemView.findViewById(R.id.tvType);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            cbSelected = itemView.findViewById(R.id.cbSelected);
            btnSerial = itemView.findViewById(R.id.btnSerial);


        }
    }


    public EquipmentSelectionAdapter(Context context, ArrayList<TrailerSelectionDO> trailerSelectionDOS) {
        this.context = context;
        this.trailerSelectionDOS = trailerSelectionDOS;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.equipment_selection_data, parent, false);

        return new MyViewHolder(itemView);
    }

    private ArrayList<TrailerSelectionDO> selectedDOs = new ArrayList<>();

    public ArrayList<TrailerSelectionDO> getSelectedEquiomentDOs() {
        return selectedDOs;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TrailerSelectionDO trailerSelectionDO = trailerSelectionDOS.get(position);
//        final VRSelectionDO vrSelectionDO = new VRSelectionDO();
//        if(trailerSelectionDO.selected){
//            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.app_color_green));
//        }else{
//            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.gray));
//        }
        if ((position % 2 == 0)) {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lg));
        } else {
            holder.itemView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.edit_text_background_lsg));
        }
        holder.tvTrailer.setText(trailerSelectionDO.trailer);
        holder.tvDescription.setText(trailerSelectionDO.trailerDescription);

        holder.tvAxle.setText("" + trailerSelectionDO.link);
        holder.tvType.setText("" + trailerSelectionDO.type);

        holder.tvMass.setText("" + trailerSelectionDO.mass + " " + trailerSelectionDO.volumeUnit);

        holder.tvVolume.setText("" + trailerSelectionDO.volume + " " + trailerSelectionDO.massUnit);
        holder.cbSelected.setOnCheckedChangeListener(null);
        holder.btnSerial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, SerializationActivity.class);
                intent.putExtra("P_ID",trailerSelectionDO.trailer);
                intent.putExtra("PRODUCT",trailerSelectionDO.trailerDescription);
                intent.putExtra("PRODUCTDO", trailerSelectionDO);
                ((BaseActivity) context).startActivityForResult(intent,10);
            }
        });

        holder.cbSelected.setChecked(trailerSelectionDO.isProductAdded);
        holder.cbSelected.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                trailerSelectionDO.isProductAdded = isChecked;
                if (isChecked) {
                    selectedDOs.add(trailerSelectionDO);

//                    if(selectedDOs.size()<2){
//                        selectedDOs.add(trailerSelectionDO);
//
//                    }else {
//                        holder.cbSelected.setChecked(false);
//                        ((BaseActivity)context).showToast(context.getResources().getString(R.string.max_2));
//                    }
                } else {
                    selectedDOs.remove(trailerSelectionDO);
                }
            }
        });

//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    for(int i=0; i<trailerSelectionDOS.size();i++)
//                    {
//                        trailerSelectionDOS.get(i).selected=false;
//                    }
//                        trailerSelectionDO.selected=true;
//                        notifyDataSetChanged();
//
//
//                }
//            });
    }

    @Override
    public int getItemCount() {
        return trailerSelectionDOS != null ? trailerSelectionDOS.size() : 10;
//        return 10;

    }

}
