package com.tbs.generic.vansales.database.tables;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by VenuAppasani on 12/12/18.
 * Copyright (C) 2018 TBS - All Rights Reserved
 */
public class LoadVehicleStockTable {

    public static final String TABLE_SCHEDULE_PRODUCTS_LIST = "schedule_products";
    public static final String TABLE_NON_SCHEDULE_PRODUCTS_LIST = "non_schedule_products";
    public static final String TABLE_RETURN_PRODUCTS_LIST = "return_products";

    public static final String COLUMN_ID = "id"; //Auto increment
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_ITEM_ID = "item_id";
    public static final String COLUMN_ITEM_DESC = "item_desc";
    public static final String COLUMN_ITEM_QUANTITY = "item_quantity";
    public static final String STOCK_UNIT = "stock_unit";
    public static final String COLUMN_ITEM_WEIGHT = "item_weight";
    public static final String COLUMN_ITEM_MASS = "item_mass";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_OTHER = "other";
    public static final String SHIPMENT_NUMBER = "shipmentNumber";

    private static final String CREATE_QUERY = "CREATE TABLE " + TABLE_SCHEDULE_PRODUCTS_LIST + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_USER_ID + " TEXT, " +
            COLUMN_ITEM_ID + " TEXT, " +
            COLUMN_ITEM_DESC + " TEXT, " +
            COLUMN_ITEM_QUANTITY + " TEXT, " +
            STOCK_UNIT + " TEXT, " +
            COLUMN_ITEM_WEIGHT + " TEXT, " +
            COLUMN_ITEM_MASS + " TEXT, " +
            COLUMN_STATUS + " TEXT, " +
            COLUMN_OTHER + " TEXT,"+
            SHIPMENT_NUMBER + " TEXT)";
    private static final String Create_TABLE_VEHICLE_NON_SCHEDULE = "CREATE TABLE " + TABLE_NON_SCHEDULE_PRODUCTS_LIST + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_USER_ID + " TEXT, " +
            COLUMN_ITEM_ID + " TEXT, " +
            COLUMN_ITEM_DESC + " TEXT, " +
            COLUMN_ITEM_QUANTITY + " TEXT, " +
            STOCK_UNIT + " TEXT, " +
            COLUMN_ITEM_WEIGHT + " TEXT, " +
            COLUMN_ITEM_MASS + " TEXT, " +
            COLUMN_STATUS + " TEXT, " +
            COLUMN_OTHER + " TEXT,"+
            SHIPMENT_NUMBER + " TEXT)";

    private static final String Create_Table_Return_Products = "CREATE TABLE " + TABLE_RETURN_PRODUCTS_LIST + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_ITEM_ID + " TEXT, " +
            COLUMN_ITEM_DESC + " TEXT, " +
            COLUMN_ITEM_QUANTITY + " TEXT, " +
            STOCK_UNIT + " TEXT, " +
            SHIPMENT_NUMBER + " TEXT)";

    public static void create(SQLiteDatabase database) {
        database.execSQL(CREATE_QUERY);
        database.execSQL(Create_TABLE_VEHICLE_NON_SCHEDULE);
        database.execSQL(Create_Table_Return_Products);
    }

    public static void upgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        //TODO: write upgrade query here
        //TODO: should be written in v2 if required
    }
}