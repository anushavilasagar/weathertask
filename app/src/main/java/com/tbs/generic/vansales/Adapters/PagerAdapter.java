package com.tbs.generic.vansales.Adapters;

/**
 * Created by sandy on 2/15/2018.
 */

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.tbs.generic.vansales.Model.PickUpDo;
import com.tbs.generic.vansales.fragments.MapFragment;
import com.tbs.generic.vansales.fragments.RouteListFragmment;

import java.util.ArrayList;

public class PagerAdapter extends FragmentStatePagerAdapter  {

    private ArrayList<PickUpDo> pickUpDos;
    public PagerAdapter(FragmentManager fm, ArrayList<PickUpDo> pickUpDos) {
        super(fm);
        this.pickUpDos = pickUpDos;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                RouteListFragmment vegFragment = new RouteListFragmment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("RoutList", pickUpDos);
                vegFragment.setArguments(bundle);
                return vegFragment;
            case 1:
                MapFragment nonVegFragment = new MapFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("RoutList", pickUpDos);
                nonVegFragment.setArguments(bundle1);
                return nonVegFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }


}
