package com.tbs.generic.vansales.Activitys

import android.app.Dialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.tbs.generic.vansales.Model.*
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.utils.PreferenceUtils
import androidx.drawerlayout.widget.DrawerLayout
import android.view.Gravity
import com.tbs.generic.vansales.Adapters.*
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.database.models.SaveStockDBModel
import com.tbs.generic.vansales.listeners.DBProductsListener
import com.tbs.generic.vansales.utils.Constants
import com.tbs.generic.vansales.utils.Util
import kotlin.collections.ArrayList


class AddProductActivity : BaseActivity() ,DBProductsListener {
    override fun updateData() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    lateinit var tvSelection: TextView
    lateinit var dialog: Dialog
    lateinit var pickDos: ArrayList<PickUpDo>
    lateinit var createInvoiceDO: CreateInvoiceDO
    lateinit var recycleview: androidx.recyclerview.widget.RecyclerView
    lateinit var llOrderHistory: RelativeLayout
    lateinit var productDOS: List<ProductDO>
    lateinit var DBProductsListener :DBProductsListener
    var customerId :String =""
    private lateinit var customProductDOSData: List<String>
    lateinit var btnAdd:Button
    lateinit var stockList : ArrayList<SaveStockDBModel>
    override fun initialize() {
        var llCategories = layoutInflater.inflate(R.layout.create_product_screen, null) as RelativeLayout
        llBody.addView(llCategories, LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        changeLocale()
        initializeControls()
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT)
    }

    override fun initializeControls() {
        tvScreenTitle.text = resources.getString(R.string.select_product)
        toolbar.setNavigationIcon(R.drawable.back)
        toolbar.setNavigationOnClickListener { finish() }
        recycleview = findViewById<View>(R.id.recycleview) as androidx.recyclerview.widget.RecyclerView
        recycleview.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        btnAdd = findViewById<View>(R.id.btnAdd) as Button
        if (intent.hasExtra("CustomerId")) {
            customerId = intent.extras?.getString("CustomerId").toString()
        }
        var preferenceUtils = PreferenceUtils(this)

        var driverID = preferenceUtils.getStringFromPreference(PreferenceUtils.DRIVER_ID, "QUA02")
        stockList= StorageManager.getInstance(this).getAllVechicleStockDetails(driverID)
        if (stockList!= null && stockList.size > 0) {
            val driverAdapter = OfflineDeliveryProductAdapter(this, stockList, object : ProductModifiedListner {
                override fun updateProductCount(customProductDOS: List<String>) {
                    customProductDOSData = customProductDOS
                }
            })
            recycleview.adapter = driverAdapter
        } else {
            hideLoader()
            showAlert(resources.getString(R.string.stock_not_assigned))
        }
        btnAdd.setOnClickListener {
            Util.preventTwoClick(it)
        }
    }
    interface ProductModifiedListner {
        fun updateProductCount(customProductDOS: List<String>)
    }
}