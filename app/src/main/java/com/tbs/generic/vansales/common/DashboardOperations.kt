package com.tbs.generic.vansales.common

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import com.alert.rajdialogs.ProgressDialog
import com.tbs.generic.vansales.Activitys.NewLoadVanSaleStockTabActivity
import com.tbs.generic.vansales.Activitys.ScheduleNonScheduleActivity
import com.tbs.generic.vansales.Activitys.VROdoCheckInActivity
import com.tbs.generic.vansales.Activitys.VRselectionScreen
import com.tbs.generic.vansales.Model.DriverIdMainDO
import com.tbs.generic.vansales.R
import com.tbs.generic.vansales.Requests.DriverIdRequest
import com.tbs.generic.vansales.Requests.NonScheduledDriverIdRequest
import com.tbs.generic.vansales.database.StorageManager
import com.tbs.generic.vansales.dialogs.YesOrNoDialogFragment
import com.tbs.generic.vansales.listeners.ResultListner
import com.tbs.generic.vansales.utils.CalendarUtils
import com.tbs.generic.vansales.utils.Constants
import com.tbs.generic.vansales.utils.PreferenceUtils
import com.tbs.generic.vansales.utils.Util
import com.tbs.generic.vansales.utils.Util.hideLoader
import com.tbs.generic.vansales.utils.Util.showLoader

class DashboardOperations {


//    private var progressDialog: ProgressDialog? = null

    fun checkInProcess(context: Context) {
//        progressDialog = Util.showProgressDialog(context)
        // loadScheduledRouteList(context)
        val intent = Intent(context, VROdoCheckInActivity::class.java)
        context.startActivity(intent)

    }

//    private fun loadScheduledRouteList(context: Context) {
//        val id = PreferenceUtils(context).getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
//        if (Util.isNetworkAvailable(context)) {
//            progressDialog?.show()
//            val driverListRequest = DriverIdRequest(id, context)
//            driverListRequest.setOnResultListener { isError, driverIdMainDO ->
//                //hideLoader()
//                if (isError) {
//                    // Toast.makeText(this, "Failed to get route list, please try again.", Toast.LENGTH_SHORT).show()
//                    loadNonScheduledRouteList(context, DriverIdMainDO())
//                } else {
//                    scheduleData(context, driverIdMainDO)
//                    loadNonScheduledRouteList(context, driverIdMainDO)
//
//                }
//            }
//            driverListRequest.execute()
//        } else {
//            YesOrNoDialogFragment().newInstance(context.getString(R.string.alert), context.getString(R.string.internet_connection), ResultListner { `object`, isSuccess -> }, false).show((context as AppCompatActivity).supportFragmentManager, "YesOrNoDialogFragment")
//        }
//    }


    private fun clickOnStockLoaded(context: Context) {

        val intent = Intent(context, ScheduleNonScheduleActivity::class.java)
        intent.putExtra(Constants.SCREEN_TYPE, context.getString(R.string.confirm_stock))
        context.startActivity(intent)

    }

//    private fun loadNonScheduledRouteList(context: Context, driverIdMainDOSchecdule: DriverIdMainDO) {
//        var id = PreferenceUtils(context).getStringFromPreference(PreferenceUtils.DRIVER_ID, "")
//        if (Util.isNetworkAvailable(context)) {
//            val driverListRequest = NonScheduledDriverIdRequest(id, context)
//
//            driverListRequest.setOnResultListener { isError, driverIdMainDO ->
//                if (isError) {
//                    //  Toast.makeText(this, "Failed to get route list, please try again.", Toast.LENGTH_SHORT).show()
//                    checkInProcessEntering(context)
//                } else {
//                    nonScheduleData(context, driverIdMainDO, driverIdMainDOSchecdule)
//                    checkInProcessEntering(context)
//                }
//                progressDialog?.hide()
//            }
//            driverListRequest.execute()
//        } else {
//            progressDialog?.hide()
//            YesOrNoDialogFragment().newInstance(context.getString(R.string.alert), context.getString(R.string.internet_connection), ResultListner { `object`, isSuccess -> }, false).show((context as AppCompatActivity).supportFragmentManager, "YesOrNoDialogFragment")
//
//        }
//
//    }

//    fun checkInProcessEntering(context: Context) {
//        val id = PreferenceUtils(context).getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
//        val nID = PreferenceUtils(context).getStringFromPreference(PreferenceUtils.Non_Scheduled_Route_Id, "")
//
//        if (id.isEmpty() && nID.isEmpty()) {
//
//            YesOrNoDialogFragment().newInstance(context.getString(R.string.alert),
//                    context.getString(R.string.stock_not_assigned), ResultListner { `object`, isSuccess -> }, false).show((context as AppCompatActivity).supportFragmentManager, "YesOrNoDialogFragment")
//
//        } else {
//
//            clickOnStockLoaded(context)
//        }
//    }
//
//    fun scheduleData(context: Context, scheduledRouteList: DriverIdMainDO) {
//
//        val preferenceUtils = PreferenceUtils(context)
//        val vehicleCheckInDo = StorageManager.getInstance(context).getVehicleCheckInData(context)
//        if (scheduledRouteList.driverDOS.get(0).checkinFlag == 33) {
//            preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, scheduledRouteList.driverDOS.get(0).srcNumber)
//            preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, scheduledRouteList.driverDOS.get(0).vehicleRouteId)
//            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, scheduledRouteList.driverDOS.get(0).vehicleCode)
//            preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, scheduledRouteList.driverDOS.get(0).vehicleCarrier)
//            preferenceUtils.saveString(PreferenceUtils.LOCATION, scheduledRouteList.driverDOS.get(0).location)
//            preferenceUtils.saveString(PreferenceUtils.V_PLATE, scheduledRouteList.driverDOS.get(0).plate)
//            preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, scheduledRouteList.driverDOS.get(0).nShipments)
//            if (scheduledRouteList.driverDOS.get(0).aTime.length > 0) {
//                val arrivalTime = scheduledRouteList.driverDOS.get(0).aTime.substring(Math.max(scheduledRouteList.driverDOS.get(0).aTime.length - 2, 0))
//                val departureTime = scheduledRouteList.driverDOS.get(0).dTime.substring(Math.max(scheduledRouteList.driverDOS.get(0).dTime.length - 2, 0))
//                val arrivalTime2 = scheduledRouteList.driverDOS.get(0).aTime.substring(0, 2)
//                val departureTime2 = scheduledRouteList.driverDOS.get(0).dTime.substring(0, 2)
//                preferenceUtils.saveString(PreferenceUtils.A_TIME, arrivalTime2 + ":" + arrivalTime)
//                preferenceUtils.saveString(PreferenceUtils.D_TIME, departureTime2 + ":" + departureTime)
//                val aMonth = scheduledRouteList.driverDOS.get(0).aDate.substring(4, 6)
//                val ayear = scheduledRouteList.driverDOS.get(0).aDate.substring(0, 4)
//                val aDate = scheduledRouteList.driverDOS.get(0).aDate.substring(Math.max(scheduledRouteList.driverDOS.get(0).aDate.length - 2, 0))
//                val dMonth = scheduledRouteList.driverDOS.get(0).dDate.substring(4, 6)
//                val dyear = scheduledRouteList.driverDOS.get(0).dDate.substring(0, 4)
//                val dDate = scheduledRouteList.driverDOS.get(0).dDate.substring(Math.max(scheduledRouteList.driverDOS.get(0).dDate.length - 2, 0))
//                preferenceUtils.saveString(PreferenceUtils.A_DATE, aDate + "-" + aMonth + "-" + ayear)
//                preferenceUtils.saveString(PreferenceUtils.D_DATE, dDate + "-" + dMonth + "-" + dyear)
//            }
//            if (scheduledRouteList.driverDOS.get(0).notes.length > 0) {
//                try {
//                    var s = scheduledRouteList.driverDOS.get(0).notes
//                    s = s.substring(s.indexOf("$") + 1)
//                    s = s.substring(0, s.indexOf("$"))
//                    vehicleCheckInDo.notes = s.toString().trim()
//                } catch (e: Exception) {
//                }
//            }
//            vehicleCheckInDo.checkInStatus = context.getString(R.string.checkin_vehicle)//resources.getString(R.string.checkin_vehicle)
//            vehicleCheckInDo.checkInTime = CalendarUtils.getCurrentDate(context)
//            vehicleCheckInDo.checkinTimeCaptureCheckinTime = CalendarUtils.getTime()
//            vehicleCheckInDo.checkinTimeCapturCheckinDate = CalendarUtils.getDate()
//            var routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
//            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, routeId)
//            var idd = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
//            preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, CalendarUtils.getCurrentDate(context))
//
//            preferenceUtils.saveString(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, idd)
//
//            var vCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")
//            var aTime = preferenceUtils.getStringFromPreference(PreferenceUtils.A_TIME, "")
//            var aDate = preferenceUtils.getStringFromPreference(PreferenceUtils.A_DATE, "")
//            var dDate = preferenceUtils.getStringFromPreference(PreferenceUtils.D_DATE, "")
//            var dTime = preferenceUtils.getStringFromPreference(PreferenceUtils.D_TIME, "")
//            var vPlate = preferenceUtils.getStringFromPreference(PreferenceUtils.V_PLATE, "")
//            var nShipments = preferenceUtils.getStringFromPreference(PreferenceUtils.N_SHIPMENTS, "")
//
//            preferenceUtils.saveString(PreferenceUtils.CVEHICLE_CODE, vCode)
//            preferenceUtils.saveString(PreferenceUtils.CA_TIME, aTime)
//            preferenceUtils.saveString(PreferenceUtils.CA_DATE, aDate)
//            preferenceUtils.saveString(PreferenceUtils.CD_DATE, dDate)
//            preferenceUtils.saveString(PreferenceUtils.CD_TIME, dTime)
//            preferenceUtils.saveString(PreferenceUtils.CV_PLATE, vPlate)
//            preferenceUtils.saveString(PreferenceUtils.CN_SHIPMENTS, nShipments)
//            preferenceUtils.saveString(PreferenceUtils.STATUS, "" + context.getString(R.string.checkin_vehicle))
//            preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, context.getString(R.string.checkin_vehicle))
//            vehicleCheckInDo.loadStock = context.getString(R.string.loaded_stock)
//            vehicleCheckInDo.checkinTimeCaptureStockLoadingTime = CalendarUtils.getTime()
//            vehicleCheckInDo.checkinTimeCaptureStockLoadingDate = CalendarUtils.getDate()
////                    refreshMenuAdapter()
////                    populateExpandableList();
//            StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//        } else {
////            llConfirmStock.setBackgroundColor(resources.getColor(R.color.white))
////            llConfirmStock.setClickable(true)
////            llConfirmStock.setEnabled(true)
////            llCheckIn.setBackgroundColor(resources.getColor(R.color.white))
////            llCheckIn.setClickable(true)
////            llCheckIn.setEnabled(true)
//        }
//
//        if (scheduledRouteList != null
//                && scheduledRouteList.driverDOS.size > 0
//                && scheduledRouteList.driverDOS.get(0) != null
//                && !scheduledRouteList.driverDOS.get(0).vehicleRouteId.equals("")) {
//            vehicleCheckInDo.scheduledRootId = scheduledRouteList.driverDOS.get(0).vehicleRouteId
//            preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, scheduledRouteList.driverDOS.get(0).srcNumber)
//            preferenceUtils.saveString(PreferenceUtils.VEHICLE_ROUTE_ID, scheduledRouteList.driverDOS.get(0).vehicleRouteId)
//            preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, scheduledRouteList.driverDOS.get(0).vehicleCode)
//            preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, scheduledRouteList.driverDOS.get(0).vehicleCarrier)
//            preferenceUtils.saveString(PreferenceUtils.LOCATION, scheduledRouteList.driverDOS.get(0).location)
//            preferenceUtils.saveString(PreferenceUtils.VR_ID, scheduledRouteList.driverDOS.get(0).vehicleRouteId)
//            preferenceUtils.saveString(PreferenceUtils.V_PLATE, scheduledRouteList.driverDOS.get(0).plate)
//            preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, scheduledRouteList.driverDOS.get(0).nShipments)
//            if (scheduledRouteList.driverDOS.get(0).aTime.length > 0) {
//                val arrivalTime = scheduledRouteList.driverDOS.get(0).aTime.substring(Math.max(scheduledRouteList.driverDOS.get(0).aTime.length - 2, 0))
//                val departureTime = scheduledRouteList.driverDOS.get(0).dTime.substring(Math.max(scheduledRouteList.driverDOS.get(0).dTime.length - 2, 0))
//                val arrivalTime2 = scheduledRouteList.driverDOS.get(0).aTime.substring(0, 2)
//                val departureTime2 = scheduledRouteList.driverDOS.get(0).dTime.substring(0, 2)
//                preferenceUtils.saveString(PreferenceUtils.A_TIME, arrivalTime2 + ":" + arrivalTime)
//                preferenceUtils.saveString(PreferenceUtils.D_TIME, departureTime2 + ":" + departureTime)
//                val aMonth = scheduledRouteList.driverDOS.get(0).aDate.substring(4, 6)
//                val ayear = scheduledRouteList.driverDOS.get(0).aDate.substring(0, 4)
//                val aDate = scheduledRouteList.driverDOS.get(0).aDate.substring(Math.max(scheduledRouteList.driverDOS.get(0).aDate.length - 2, 0))
//                val dMonth = scheduledRouteList.driverDOS.get(0).dDate.substring(4, 6)
//                val dyear = scheduledRouteList.driverDOS.get(0).dDate.substring(0, 4)
//                val dDate = scheduledRouteList.driverDOS.get(0).dDate.substring(Math.max(scheduledRouteList.driverDOS.get(0).dDate.length - 2, 0))
//                preferenceUtils.saveString(PreferenceUtils.A_DATE, aDate + "-" + aMonth + "-" + ayear)
//                preferenceUtils.saveString(PreferenceUtils.D_DATE, dDate + "-" + dMonth + "-" + dyear)
//            }
//            if (scheduledRouteList.driverDOS.get(0).notes.length > 0) {
//                try {
//                    var s = scheduledRouteList.driverDOS.get(0).notes
//                    s = s.substring(s.indexOf("$") + 1)
//                    s = s.substring(0, s.indexOf("$"))
//                    vehicleCheckInDo.notes = "" + s.toString().trim()
//                } catch (e: Exception) {
//                }
//                StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//            }
//            StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//
//        }
//    }
//
//
//    fun nonScheduleData(context: Context, nonScheduledRouteList: DriverIdMainDO, driverIdMainDOSchecdule: DriverIdMainDO) {
//        if (nonScheduledRouteList != null && nonScheduledRouteList.driverDOS.size > 0) {
//            val vehicleCheckInDo = StorageManager.getInstance(context).getVehicleCheckInData(context)
//            val preferenceUtils = PreferenceUtils(context)
//            if (nonScheduledRouteList.driverDOS.get(0).checkinFlag == 33) {
//                preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//                vehicleCheckInDo.nonScheduledRootId = nonScheduledRouteList.driverDOS.get(0).vehicleRouteId
//                val vehicleCheckInDo = StorageManager.getInstance(context).getVehicleCheckInData(context)
//                var id = ""
//                preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
//                preferenceUtils.saveString(PreferenceUtils.LOCATION, nonScheduledRouteList.driverDOS.get(0).location)
//                preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//                StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//                if (driverIdMainDOSchecdule.driverDOS.size == 0) {
//                    preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//                    preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
//                    preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
//                    preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, nonScheduledRouteList.driverDOS.get(0).vehicleCarrier)
//                    preferenceUtils.saveString(PreferenceUtils.VR_ID, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//                    preferenceUtils.saveString(PreferenceUtils.V_PLATE, nonScheduledRouteList.driverDOS.get(0).plate)
//                    preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, nonScheduledRouteList.driverDOS.get(0).nShipments)
//                    StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//                    vehicleCheckInDo.checkInStatus = context.getString(R.string.checkin_vehicle)//resources.getString(R.string.checkin_vehicle)
//                    vehicleCheckInDo.checkInTime = CalendarUtils.getCurrentDate(context)
//                    vehicleCheckInDo.checkinTimeCaptureCheckinTime = CalendarUtils.getTime()
//                    vehicleCheckInDo.checkinTimeCapturCheckinDate = CalendarUtils.getDate()
//                    var routeId = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
//                    preferenceUtils.saveString(PreferenceUtils.VEHICLE_CHECKED_ROUTE_ID, routeId)
//                    var idd = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_ROUTE_ID, "")
//                    preferenceUtils.saveString(PreferenceUtils.CHECKIN_DATE, CalendarUtils.getCurrentDate(context))
//                    preferenceUtils.saveString(PreferenceUtils.BASE_VEHICLE_ROUTE_ID, idd)
//                    var vCode = preferenceUtils.getStringFromPreference(PreferenceUtils.VEHICLE_CODE, "")
//                    var aTime = preferenceUtils.getStringFromPreference(PreferenceUtils.A_TIME, "")
//                    var aDate = preferenceUtils.getStringFromPreference(PreferenceUtils.A_DATE, "")
//                    var dDate = preferenceUtils.getStringFromPreference(PreferenceUtils.D_DATE, "")
//                    var dTime = preferenceUtils.getStringFromPreference(PreferenceUtils.D_TIME, "")
//                    var vPlate = preferenceUtils.getStringFromPreference(PreferenceUtils.V_PLATE, "")
//                    var nShipments = preferenceUtils.getStringFromPreference(PreferenceUtils.N_SHIPMENTS, "")
//                    preferenceUtils.saveString(PreferenceUtils.CVEHICLE_CODE, vCode)
//                    preferenceUtils.saveString(PreferenceUtils.CA_TIME, aTime)
//                    preferenceUtils.saveString(PreferenceUtils.CA_DATE, aDate)
//                    preferenceUtils.saveString(PreferenceUtils.CD_DATE, dDate)
//                    preferenceUtils.saveString(PreferenceUtils.CD_TIME, dTime)
//                    preferenceUtils.saveString(PreferenceUtils.CV_PLATE, vPlate)
//                    preferenceUtils.saveString(PreferenceUtils.CN_SHIPMENTS, nShipments)
//                    preferenceUtils.saveString(PreferenceUtils.STATUS, "" + context.getString(R.string.checkin_vehicle))
//                    preferenceUtils.saveString(PreferenceUtils.CHECK_IN_STATUS, context.getString(R.string.checkin_vehicle))
//                    vehicleCheckInDo.loadStock = context.getString(R.string.loaded_stock)
//                    vehicleCheckInDo.checkinTimeCaptureStockLoadingTime = CalendarUtils.getTime()
//                    vehicleCheckInDo.checkinTimeCaptureStockLoadingDate = CalendarUtils.getDate()
////                            refreshMenuAdapter()/
////                    populateExpandableList();
//                    StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//                }
//            } else {
////                llConfirmStock.setBackgroundColor(resources.getColor(R.color.white))
////                llConfirmStock.setClickable(true)
////                llConfirmStock.setEnabled(true)
////                llCheckIn.setBackgroundColor(resources.getColor(R.color.white))
////                llCheckIn.setClickable(true)
////                llCheckIn.setEnabled(true)
//            }
//            preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//            vehicleCheckInDo.nonScheduledRootId = nonScheduledRouteList.driverDOS.get(0).vehicleRouteId
//            preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
//            preferenceUtils.saveString(PreferenceUtils.LOCATION, nonScheduledRouteList.driverDOS.get(0).location)
//            preferenceUtils.saveString(PreferenceUtils.DOC_NUMBER, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//            StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//            if (driverIdMainDOSchecdule.driverDOS != null && driverIdMainDOSchecdule.driverDOS.size > 0
//                    && driverIdMainDOSchecdule.driverDOS.get(0).vehicleRouteId.isEmpty()) {
//                preferenceUtils.saveString(PreferenceUtils.Non_Scheduled_Route_Id, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//                preferenceUtils.saveString(PreferenceUtils.VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
//                preferenceUtils.saveString(PreferenceUtils.NON_VEHICLE_CODE, nonScheduledRouteList.driverDOS.get(0).vehicleCode)
//                preferenceUtils.saveString(PreferenceUtils.CARRIER_ID, nonScheduledRouteList.driverDOS.get(0).vehicleCarrier)
//                preferenceUtils.saveString(PreferenceUtils.VR_ID, nonScheduledRouteList.driverDOS.get(0).vehicleRouteId)
//                preferenceUtils.saveString(PreferenceUtils.V_PLATE, nonScheduledRouteList.driverDOS.get(0).plate)
//                preferenceUtils.saveString(PreferenceUtils.N_SHIPMENTS, nonScheduledRouteList.driverDOS.get(0).nShipments)
//                StorageManager.getInstance(context).insertCheckInData(context, vehicleCheckInDo)
//            }
//        }
//
//    }

}